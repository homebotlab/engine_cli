engine-cli
==========



[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/engine-cli.svg)](https://npmjs.org/package/engine-cli)
[![Downloads/week](https://img.shields.io/npm/dw/engine-cli.svg)](https://npmjs.org/package/engine-cli)
[![License](https://img.shields.io/npm/l/engine-cli.svg)](https://github.com/engine/engine-cli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g engine-cli
$ engine-cli COMMAND
running command...
$ engine-cli (-v|--version|version)
engine-cli/0.0.0 win32-x64 node-v10.15.0
$ engine-cli --help [COMMAND]
USAGE
  $ engine-cli COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`engine-cli help [COMMAND]`](#engine-cli-help-command)
* [`engine-cli init`](#engine-cli-init)

## `engine-cli help [COMMAND]`

display help for engine-cli

```
USAGE
  $ engine-cli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.0/src\commands\help.ts)_

## `engine-cli init`

Initializes a new engine project

```
USAGE
  $ engine-cli init

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src\commands\init.js](https://github.com/engine/engine-cli/blob/v0.0.0/src\commands\init.js)_
<!-- commandsstop -->
