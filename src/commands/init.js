/* eslint-disable eol-last */
/* eslint-disable new-cap */
/* eslint-disable node/no-extraneous-require */
const fs = require('fs')
const path = require('path')
const Git = require('nodegit')
const execa = require('execa')
const rimraf = require("rimraf")
const {Command} = require('@oclif/command')
const {cli} = require('cli-ux')

class InitCommand extends Command {
  async run() {
    const {argv} = this.parse(InitCommand)

    // Create new project folder
    const projectName = argv[0] ? argv[0] : await cli.prompt('What is project name ?')
    const projectPath = path.join(process.cwd(), projectName)
    fs.mkdirSync(projectPath)

    // Get credentials to git access
    const username = await cli.prompt('What is your username in Bitbucket')
    const password = await cli.prompt('What is your password?', {type: 'hide'})
    const gitCredentials = {
      fetchOpts: {
        callbacks: {
          credentials: () => Git.Cred.userpassPlaintextNew(username, password),
        },
      },
    }

    // Git urls
    const gitSkeletonUrl = `https://${username}@bitbucket.org/homebotlab/engine_skeleton.git`

    // Repository cloning
    cli.action.start('Cloning engine-sketelon repository')
    await Git.Clone(gitSkeletonUrl, projectPath, gitCredentials)
    cli.action.stop()

    // Git configuration
    cli.action.start('Git configuration')
    await execa.command('git remote remove origin', { cwd: projectPath, })
    await execa.command('git submodule init', { cwd: projectPath, })
    cli.action.stop()
    
    // Install dependencies
    cli.action.start('Install dependencies')
    await execa.command('npm install', {
      cwd: projectPath,
    })
    cli.action.stop()
    
    this.log(`Project successfully initialized

1) Before starting, configure environment variables by creating a .env file. 
An example of its contents is in the .env.example file.

2) Load submodule data
git submodule update

3) Then run the scripts to load the data into the database:
npm run load-regions
npm run load-users
    `)
  }
}

InitCommand.description = `Initializes a new engine project
...
Extra documentation goes here
`

module.exports = InitCommand